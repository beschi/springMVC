Master Spring MVC
-----------------------------------------------------------
Enabling redis profile
-----------------------------------------------------------
1. Add the following in JVM option in the run configuration of IDE (eclipse)
	-Dspring.profiles.active=redis
	
2. Generate the JAR with gradlew build and launch it with the following command:
	java -Dserver.port=$PORT -Dspring.profiles.active=redis -jar app.jar
	
3. Alternatively, can launch it with Gradle in Bash, as follows:
	SPRING_PROFILES_ACTIVE=redis ./gradlew bootRun

------------------------------------------------------------
Key Creation
------------------------------------------------------------
keytool -genkey -alias masterspringmvc -keyalg RSA -keystore src/main/resources/masterspringmvc.keystore