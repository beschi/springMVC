package org.am.dba.masterspringmvc.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.LocalDate;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 13, 2017
 * @purpose Custom annotation for Date validation
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy=PastLocalDate.PastValidator.class)
@Documented
public @interface PastLocalDate {
  String message() default "{javax.validation.constraints.Past.message}";
  Class<?>[] groups() default {};
  Class<? extends Payload>[] payload() default {};
  
  class PastValidator implements ConstraintValidator<PastLocalDate, LocalDate>{

	@Override
	public void initialize(PastLocalDate constraintAnnotation) {}

	@Override
	public boolean isValid(LocalDate value,
		ConstraintValidatorContext context) {
	  return value == null || value.isBefore(LocalDate.now());
	}
	
  }
}
