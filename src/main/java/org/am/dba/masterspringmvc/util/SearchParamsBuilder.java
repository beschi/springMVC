package org.am.dba.masterspringmvc.util;

import org.springframework.social.twitter.api.SearchParameters;

public class SearchParamsBuilder {
  

  public static SearchParameters createSearchParam(String searchType, String taste) {
	  SearchParameters.ResultType resultType = getResultType(searchType);
	  SearchParameters searchParameters = new SearchParameters(taste);
	  searchParameters.resultType(resultType);
	  searchParameters.count(3);
	  return searchParameters;
  }
  
  public static SearchParameters.ResultType getResultType(String searchType) {
	for (SearchParameters.ResultType knownType : SearchParameters.ResultType.values()) {
	  if (knownType.name().equalsIgnoreCase(searchType)) {
		return knownType;
	  }
	}
	return SearchParameters.ResultType.RECENT;
  }

}
