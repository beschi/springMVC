package org.am.dba.masterspringmvc.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.am.dba.masterspringmvc.component.UserProfileSession;
import org.am.dba.masterspringmvc.dto.ProfileForm;
import org.am.dba.masterspringmvc.util.LocalDateFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ProfileController {
  
  private UserProfileSession userProfileSession;
  
  @Autowired
  public ProfileController(UserProfileSession ups){
	this.userProfileSession = ups;
  }

  @ModelAttribute("dateFormat")
  public String localeFormat(Locale locale) {
	return LocalDateFormatter.getPattern(locale);
  }

  @RequestMapping("/profile")
  public String displayProfile(ProfileForm profileForm) {
	return "profile/profilePage";
  }

  @RequestMapping(value = "/profile", params = {"save"}, method = RequestMethod.POST)
  public String saveProfile(@Valid ProfileForm profileForm,
	  BindingResult bindingResult) {
	if (bindingResult.hasErrors()){
	  return "profile/profilePage";
	}
	
	userProfileSession.saveForm(profileForm);
	return "redirect:/search/mixed;keywords=" + String.join(",",profileForm.getTastes());
	//return "redirect:/profile";
  }

  @RequestMapping(value = "/profile", params = { "addTaste" })
  public String addTaste(ProfileForm profileForm) {
	profileForm.getTastes().add(null);
	return "profile/profilePage";
  }

  @RequestMapping(value = "/profile", params = {"removeTaste"})
  public String removeTaste(ProfileForm profileForm, HttpServletRequest req) {
	Integer rowId = Integer.valueOf(req.getParameter("removeTaste"));
	profileForm.getTastes().remove(rowId.intValue());
	return "profile/profilePage";
  }

}
