package org.am.dba.masterspringmvc.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.am.dba.masterspringmvc.component.UserProfileSession;
import org.am.dba.masterspringmvc.propconfig.ImageUploadProperties;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ImageUploadController {
  private final Resource picturesDir;
  private final Resource anonymousImage;
  private final MessageSource messageSource;
  private final UserProfileSession userProfileSession;
  
  @Autowired
  public ImageUploadController(ImageUploadProperties properties, MessageSource messageSource, 
	  UserProfileSession userProfileSession){
	this.picturesDir = properties.getUploadPath();
	this.anonymousImage = properties.getAnonymousPath();
	this.messageSource = messageSource;
	this.userProfileSession = userProfileSession;
  }
  
  
  @RequestMapping("upload")
  public String uploadPage() {
	return "profile/uploadPage";
  }
  
  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  public String onUpload(MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {
	if (file.isEmpty() || !isImage(file)) {
	  redirectAttributes.addFlashAttribute("error", "Incorrect file.Please upload a picture.");
	  return "redirect:/profile";
	}
	Resource uploadedPicture = copyToFileSystem(file);
	userProfileSession.setPicturePath(uploadedPicture);
	return "redirect:/profile";
  }
  
  @RequestMapping(value = "/uploadedPicture")
  public void getUploadedPicture(HttpServletResponse response) throws
  IOException {
	Resource picturePath = userProfileSession.getPicturePath();
	if(picturePath == null){
	  picturePath = anonymousImage;
	}
	response.setHeader("Content-Type", URLConnection.guessContentTypeFromName(picturePath.getFilename()));
	IOUtils.copy(picturePath.getInputStream(), response.getOutputStream());
  }
  
  /**
   * 
   * @param exception
   * @purpose Handles the exception in controller itself
   */
  @ExceptionHandler(IOException.class)
  public ModelAndView handleIOException(Locale locale){
	ModelAndView mav = new ModelAndView("profile/profilePage");
	mav.addObject("error", messageSource.getMessage("upload.io.error", null, locale));
	mav.addObject("profileForm", userProfileSession.toForm());
	return mav;
  }
  
  /**
   * 
   * @param request
   * @purpose Exception is thrown from servlet container, and it is redirected to this method. 
   * Refer Webconfiguration for more details
   */
  @RequestMapping("uploadError")
  public ModelAndView onUploadError(Locale locale) {
	ModelAndView mav = new ModelAndView("profile/profilePage");
	//mav.addObject("error",request.getAttribute(WebUtils.ERROR_MESSAGE_ATTRIBUTE));
	mav.addObject("error",messageSource.getMessage("upload.file.big.error", null, locale));
	mav.addObject("profileForm", userProfileSession.toForm());
	return mav;
  }
  
  private Resource copyToFileSystem(MultipartFile file) throws IOException{
	String filename = file.getOriginalFilename();
	File tempFile = File.createTempFile("pic", getFileExtension(filename),  picturesDir.getFile());
	try (InputStream in = file.getInputStream();
	  OutputStream out = new FileOutputStream(tempFile)) {
	  IOUtils.copy(in, out);
	}
	return new FileSystemResource(tempFile);
  }
  
  private static String getFileExtension(String name) {
	return name.substring(name.lastIndexOf("."));
  }
  
  private boolean isImage(MultipartFile file) {
	return file.getContentType().startsWith("image");
	}


//moved to userprofilesession
/*  @ModelAttribute("picturePath")
  public Resource picturePath() {
	return anonymousImage;
  }*/

  
/*  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  public String onUpload(MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {
	if (file.isEmpty() || !isImage(file)) {
	  redirectAttributes.addFlashAttribute("error", "Incorrect file.Please upload a picture.");
	  return "redirect:/upload";
	}
	copyToFileSystem(file);	  
	return "profile/uploadPage";
  }*/
  

/*  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  public String onUpload(MultipartFile file, RedirectAttributes redirectAttributes, Model model) throws IOException {
	if (file.isEmpty() || !isImage(file)) {
	  redirectAttributes.addFlashAttribute("error", "Incorrect file.Please upload a picture.");
	  return "redirect:/upload";
	}
	Resource uploadedPicture = copyToFileSystem(file);
	userProfileSession.setPicturePath(uploadedPicture);
	model.addAttribute("picturePath",uploadedPicture);
	return "profile/uploadPage";
	//throw new IOException("Testing io exception");
  }
  */
  /*@RequestMapping(value = "/uploadedPicture")
  public void getUploadedPicture(HttpServletResponse response) throws
  IOException {
	response.setHeader("Content-Type", URLConnection.guessContentTypeFromName(anonymousImage.getFilename()));
	IOUtils.copy(anonymousImage.getInputStream(), response.getOutputStream());
  }*/
  
}
