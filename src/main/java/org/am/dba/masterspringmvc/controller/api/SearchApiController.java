package org.am.dba.masterspringmvc.controller.api;

import java.util.List;

import org.am.dba.masterspringmvc.dto.LightTweet;
import org.am.dba.masterspringmvc.service.ISearchService;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/search")
public class SearchApiController {
  private ISearchService searchService;

  public SearchApiController(ISearchService searchService) {
	this.searchService = searchService;
  }

  @RequestMapping(value = "{searchType}", method = RequestMethod.GET)
  public List<LightTweet> search(@PathVariable String searchType, @MatrixVariable List<String> keywords) {
	return searchService.search(searchType, keywords);
  }
}
