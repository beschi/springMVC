package org.am.dba.masterspringmvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.twitter.api.SearchResults;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
@Controller
public class TwitterController {
  
  @Autowired
  private Twitter twitter;
  
  @RequestMapping("/searchPage")
  public String searchPage() {
	return "searchPage";
  }
  
  @RequestMapping(value = "/postSearch", method = RequestMethod.POST)
  public String postSearch(HttpServletRequest request, RedirectAttributes redirectAttributes) {
	String search = request.getParameter("key");
	if (search.toLowerCase().contains("struts")) {
	  redirectAttributes.addFlashAttribute("error", "Try using spring instead!");
	  return "redirect:/";
	}
	redirectAttributes.addAttribute("key", search);
	return "redirect:searchTweet";
  }
  
  @RequestMapping(value = "/searchTweet", method = RequestMethod.GET)
  public String searchTweets(@RequestParam(defaultValue="learnspringmvc1111") String key, Model model){
	SearchResults searchResults = twitter.searchOperations().search(key);
	/*List<String> tweets = searchResults.getTweets().stream()
												   .map(Tweet::getText)
												   .collect(toList());*/
	List<Tweet> tweets = searchResults.getTweets();
	model.addAttribute("tweets", tweets);
	model.addAttribute("search", key);
	return "searchResults";
  }
}
