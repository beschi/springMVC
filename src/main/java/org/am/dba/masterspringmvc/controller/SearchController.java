package org.am.dba.masterspringmvc.controller;

import java.util.List;

import org.am.dba.masterspringmvc.dto.LightTweet;
import org.am.dba.masterspringmvc.service.ISearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SearchController {
  private ISearchService searchService;

  @Autowired
  public SearchController(ISearchService searchService) {
	this.searchService = searchService;
  }

  @RequestMapping("/search/{searchType}")
  public ModelAndView search(@PathVariable String searchType, @MatrixVariable List<String> keywords) {
	List<LightTweet> tweets = searchService.search(searchType, keywords);
	ModelAndView modelAndView = new ModelAndView("searchResults");
	modelAndView.addObject("tweets", tweets);
	modelAndView.addObject("search", String.join(",", keywords));
	return modelAndView;
  }
}
