package org.am.dba.masterspringmvc.controller;

import java.util.List;

import org.am.dba.masterspringmvc.component.UserProfileSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
  
  private UserProfileSession userProfileSession;

  @Autowired
  public HomeController(UserProfileSession userProfileSession) {
	this.userProfileSession = userProfileSession;
  }

  @RequestMapping("/")
  public String home() {
	List<String> tastes = userProfileSession.toForm().getTastes();
	if (tastes.isEmpty()) {
	  return "redirect:/profile";
	}
	return "redirect:/search/mixed;keywords=" + String.join(",", tastes);
  }

  @RequestMapping("/home")
  public String greet(@RequestParam(defaultValue="guest") String name, Model model){
	model.addAttribute("message", "Hello "+name);
	return "resultPage";
  }
}
