package org.am.dba.masterspringmvc.controller.api;

import java.util.List;

import org.am.dba.masterspringmvc.controller.repository.UserRepository;
import org.am.dba.masterspringmvc.exception.EntityNotFoundException;
import org.am.dba.masterspringmvc.modal.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class UserApiController {

  private UserRepository userRepository;

  public UserApiController(UserRepository userRepository) {
	this.userRepository = userRepository;
  }

  @RequestMapping(value = "/users", method = RequestMethod.GET)
  public ResponseEntity<List<User>> findAll() {
	if(userRepository.findAll().isEmpty()){
	  return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
	}
	return new ResponseEntity<List<User>>(userRepository.findAll(),HttpStatus.OK);
  }

  @RequestMapping(value = "/users", method = RequestMethod.POST)
  public ResponseEntity<User> createUser(@RequestBody User user) {
	HttpStatus status = HttpStatus.OK;
	if (!userRepository.exists(user.getEmail())) {
	status = HttpStatus.CREATED;
	}
	User saved = userRepository.save(user);
	return new ResponseEntity<>(saved, status);
  }

  @RequestMapping(value = "/users/{email}", method = RequestMethod.PUT)
  public ResponseEntity<User> updateUser(@PathVariable String email, @RequestBody User user) throws EntityNotFoundException{
	if (!userRepository.exists(user.getEmail())) {
	  return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	  }
	  User saved = userRepository.update(email, user);
	  return new ResponseEntity<>(saved, HttpStatus.CREATED);
  }

  @RequestMapping(value = "/users/{email}", method = RequestMethod.DELETE)
  public ResponseEntity<User> deleteUser(@PathVariable String email) throws EntityNotFoundException {
	if (!userRepository.exists(email)) {
	  return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	  }
	  userRepository.delete(email);
	  return new ResponseEntity<>(HttpStatus.OK);
  }

}
