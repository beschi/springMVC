package org.am.dba.masterspringmvc.propconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

@ConfigurationProperties(prefix="upload.images")
public class ImageUploadProperties {
  private Resource uploadPath;
  private Resource anonymousPath;
  
  public Resource getUploadPath() {
    return uploadPath;
  }
  public void setUploadPath(String uploadPath) {
    this.uploadPath = new DefaultResourceLoader().getResource(uploadPath);
  }
  public Resource getAnonymousPath() {
    return anonymousPath;
  }
  public void setAnonymousPath(String anonymousPath) {
    this.anonymousPath = new DefaultResourceLoader().getResource(anonymousPath);
  }
}
