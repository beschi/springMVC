package org.am.dba.masterspringmvc.config;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 
 * @author D Beschi Antony
 * @email  beschi.agent@gmail.com
 * @created on Jun 19, 2017
 * @purpose Enables async api execution
 */
@Configuration
@EnableAsync
public class AsyncConfiguration implements AsyncConfigurer{
  protected final Log logger = LogFactory.getLog(getClass());
  
  @Value("${async.threadPoolSize:10}")
  private int threadPoolSize;

  @Override
  public Executor getAsyncExecutor() {
	return Executors.newFixedThreadPool(threadPoolSize);
  }

  @Override
  public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
	return (exception, method, params)->logger.error("Uncaught async error", exception);
  }
  
  
  
}
