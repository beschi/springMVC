package org.am.dba.masterspringmvc.config;

import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.cache.CacheBuilder;

@Configuration
@EnableCaching
public class CacheConfiguration {
  
  @Bean
  public CacheManager cacheManager() {
	GuavaCacheManager cacheManager = new GuavaCacheManager("searches");
	cacheManager.setCacheBuilder(CacheBuilder.newBuilder().softValues().expireAfterWrite(1, TimeUnit.MINUTES));
	return cacheManager;
  }

/*  @Bean
  public CacheManager cacheManager() {
	SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
	simpleCacheManager.setCaches(Arrays.asList(new ConcurrentMapCache("searches")));
	return simpleCacheManager;
  }*/

}
