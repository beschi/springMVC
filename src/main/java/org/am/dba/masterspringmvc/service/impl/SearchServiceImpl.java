package org.am.dba.masterspringmvc.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.am.dba.masterspringmvc.dto.LightTweet;
import org.am.dba.masterspringmvc.service.ISearchService;
import org.am.dba.masterspringmvc.service.SearchCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("!async")
public class SearchServiceImpl implements ISearchService {
  
  private SearchCache searchCache;

  @Autowired
  public SearchServiceImpl(SearchCache searchCache) {
	this.searchCache = searchCache;
  }
  
  public List<LightTweet> search(String searchType, List<String> keywords) {
	List<LightTweet> results = keywords.stream()
		.flatMap(keyword ->searchCache.fetch(searchType, keyword).stream())
		.collect(Collectors.toList());
	return results;
  }

}
