package org.am.dba.masterspringmvc.service;

import java.util.List;

import org.am.dba.masterspringmvc.dto.LightTweet;

public interface ISearchService {
  public List<LightTweet> search(String searchType, List<String> keywords);
}
