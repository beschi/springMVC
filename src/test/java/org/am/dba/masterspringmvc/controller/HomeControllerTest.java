package org.am.dba.masterspringmvc.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.am.dba.masterspringmvc.component.UserProfileSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers= HomeController.class, secure=true)
public class HomeControllerTest {
  
  @MockBean
  private UserProfileSession userProfileSession;
  
  @Autowired
  private MockMvc mockMvc; 
  
  @Test
  public void test() throws Exception {
	this.mockMvc.perform(get("/"))
				.andDo(print())
				.andExpect(status().isUnauthorized());
  }

}
